/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package provarecvitor.model;

import br.senac.componente.model.BaseDAO;
import br.senac.componentes.db.ConexaoDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor
 */
public class CarroDAO implements BaseDAO<Carro, Integer>{

	/*Usar Statement ou PreparedStatement */
    @Override
    public Carro getPorId(Integer id) throws SQLException {
        String sql = "select * from carro c "
                    + " where c.idcarro = " + id;
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        
        while(rs.next()){
            return getCarro(rs);
        }
        return null;
    }

	/*Usar Statement ou PreparedStatement */
    @Override
    public boolean excluir(Integer id) throws SQLException {
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        stm.executeUpdate("delete from carro where idcarro = " + id);
        return true;
    }

	/*Usar Statement */
    @Override
    public Boolean alterar(Carro carro) throws SQLException {
        String sql = "UPDATE `projeto`.`carro`\n" +
                "SET\n" +
                "`nmcarro` = ?,\n" +
                "`dtlancamento` = ?,\n" +
                "`flsaiulinha` = ?,\n" +
                "`tpcampo` = ? \n" +
                "WHERE `idcarro` = ? ";
        Connection conn = ConexaoDB.getInstance().getConnection();
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, carro.getNmCarro());
        pstm.setDate(2, new java.sql.Date(carro.getDtLancamento().getTime()));
        pstm.setInt(3,(carro.isFlSaiuLinha()) );
        pstm.setString(4, carro.getTpCambio());
        pstm.setInt(5, carro.getIdCarro());
        pstm.executeUpdate();
        return true;
    }

	/*Usar PreparedStatement */
    @Override
    public Integer inserir(Carro carro) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dataExp = sdf.format(carro.getDtLancamento());
        
        String sql = "INSERT INTO `projeto`.`carro`\n" +
                     "(`nmcarro`, `dtlancamento`,`flsaiulinha`, `tpcambio`)\n" +
                      "VALUES ('"+ carro.getNmCarro()+"', "
                        + " {d '"+dataExp+"'}, "
                        + "  '"+carro.isFlSaiuLinha()+"', "
                        + "  '"+carro.getTpCambio() +")";
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        int regCriados = stm.executeUpdate(sql, 
                                            Statement.RETURN_GENERATED_KEYS);
        ResultSet rsPK = stm.getGeneratedKeys();   //Obter PK gerada
        if (rsPK.next()) {
              return rsPK.getInt(1);
        }
        throw new RuntimeException("Erro inesperado ao incluir CARRO!");
    }

	/*Usar Statement ou PreparedStatement */
    public List<Carro> listarTodos() throws SQLException {
                String sql = "select * from carro";
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        List<Carro> lista = new ArrayList<>();
        while(rs.next()){
            lista.add(getCarro(rs));
        }
        return lista;
    }
    /* Transforma uma linha do ResultSet num objeto do tipo Carro*/
    private Carro getCarro(ResultSet rs) throws SQLException{
        Carro carro = new Carro();
        carro.setIdCarro(rs.getInt("idcarro"));
        carro.setNmCarro(rs.getString("nmcarro"));
        carro.setDtLancamento(rs.getDate("dtlancamento"));
        carro.setTpCambio(rs.getString("tpcambio"));
        carro.setFlSaiuLinha(rs.getBoolean("flsaiulinha"));

        return carro;
    }
}
